package main.java;

import com.amazon.ask.Skill;
import com.amazon.ask.Skills;
import com.amazon.ask.SkillStreamHandler;
import main.java.handlers.*;

public class AlexaSkillCryptoFactsStreamHandler extends SkillStreamHandler {

    private static Skill getSkill() {
        return Skills.standard()
                .addRequestHandlers(
                        new CancelandStopIntentHandler(),
                        new FactIntentHandler(),
                        new HelpIntentHandler(),
                        new LaunchRequestHandler(),
                        new SessionEndedRequestHandler(),
                        new FallBackIntentHandler())
                // Add your skill id below
                .withSkillId("amzn1.ask.skill.4e9e9096-382c-499d-bae7-773054be091a")
                .build();
    }

    public AlexaSkillCryptoFactsStreamHandler() {
        super(getSkill());
    }

}
