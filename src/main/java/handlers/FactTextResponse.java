package main.java.handlers;

public class FactTextResponse {
    private String factText;


    public FactTextResponse(String factText) {
        this.factText = factText;
    }

    public FactTextResponse() {
    }

    @Override
    public String toString() {
        return "FactTextResponse{" +
                "factText='" + factText + '\'' +
                '}';
    }

    public String getFactText() {
        return factText;
    }

    public void setFactText(String factText) {
        this.factText = factText;
    }
}
