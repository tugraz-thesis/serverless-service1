package main.java.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.interfaces.display.*;
import com.amazon.ask.model.services.Serializer;
import com.amazon.ask.util.JacksonSerializer;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.*;

import static com.amazon.ask.request.Predicates.intentName;

public class FactIntentHandler implements RequestHandler {


    private final Serializer serializer =  new JacksonSerializer();

    private static final Logger log = LoggerFactory.getLogger(FactIntentHandler.class);


    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("FactIntent").or(intentName("AMAZON.YesIntent")));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {

        // --------------------------------------------------
        //common lambda configuration
        Regions region = Regions.fromName("eu-west-1");
        AWSLambdaClientBuilder builder = AWSLambdaClientBuilder.standard()
                .withRegion(region);
        AWSLambda client = builder.build();


        // --------------------------------------------------
        // call lambda function get random fact index (service 2)
        // synchronously

        InvokeRequest reqId = new InvokeRequest()
                .withFunctionName("CryptcurrencyFactAlexaService2")
                .withPayload("")
                .withInvocationType("RequestResponse");
        InvokeResult resultId = client.invoke(reqId);

        ByteBuffer byteBufferId =  resultId.getPayload();
        String rawJsonId = null;
        try {
            rawJsonId = new String(byteBufferId.array(), "UTF-8");
        }catch (Exception e) {

        }

        RandomFactIdResponse randomFactIdResponse = (RandomFactIdResponse)this.serializer.deserialize(rawJsonId, RandomFactIdResponse.class);
        int index = randomFactIdResponse.getId();

        // --------------------------------------------------
        // call lambda function get fact text (service 3)
        // synchronously

        System.out.println("rawJsonId:" + rawJsonId);


        InvokeRequest reqFactText = new InvokeRequest()
                .withFunctionName("CryptcurrencyFactAlexaService3")
                .withPayload(rawJsonId)
                .withInvocationType("RequestResponse");
        InvokeResult resultFactText = client.invoke(reqFactText);

        ByteBuffer byteBufferFactText =  resultFactText.getPayload();
        String rawJsonFactText = null;
        try {
            rawJsonFactText = new String(byteBufferFactText.array(), "UTF-8");
        }catch (Exception e) {

        }

        FactTextResponse factTextResponse= (FactTextResponse) this.serializer.deserialize(rawJsonFactText, FactTextResponse.class);
        String primaryText = factTextResponse.getFactText();


        // --------------------------------------------------
        // call lambda function for service 4
        // synchronously

        InvokeRequest req = new InvokeRequest()
                .withFunctionName("CryptocurrencyFactAlexaService4")
                .withPayload("{\"id\": \"1\" }")
                .withInvocationType("RequestResponse");
        InvokeResult result = client.invoke(req);

        System.out.println("result");
        System.out.println(result.toString());

        ByteBuffer byteBuffer =  result.getPayload();

        String rawJson = null;

        try {
            rawJson = new String(byteBuffer.array(), "UTF-8");
        }catch (Exception e) {

        }

        String url ="";

        ImageUrlResponse imageUrlResponse = (ImageUrlResponse)this.serializer.deserialize(rawJson, ImageUrlResponse.class);
        url = imageUrlResponse.getUrl();

        // --------------------------------------------------
        //String imageUrl = images.get(key);
        //Image image = getImage(imageUrl);

        String title = "Airplane Facts";
        //FIXME: If you would like to display additional text, please set the secondary text accordingly
        String secondaryText = "";
        String speechText = "<speak> " + primaryText + "<break time=\"1s\"/>  Would you like to hear another cryptocurrency fact?" + " </speak>";


        Image image = getImage(url);

        Template template = getBodyTemplate3(title, primaryText, secondaryText, image);

        // Device supports display interface
        if(null!=input.getRequestEnvelope().getContext().getDisplay()) {
            return input.getResponseBuilder()
                    .withSpeech(speechText)
                    .withSimpleCard(title, primaryText)
                    .addRenderTemplateDirective(template)
                    .withReprompt(speechText)
                    .build();
        } else {
            // Headless device
            return input.getResponseBuilder()
                    .withSpeech(speechText)
                    .withSimpleCard(title, primaryText)
                    .withReprompt(speechText)
                    .build();
        }
    }

    /**
     * Helper method to create a body template 3
     * @param title the title to be displayed on the template
     * @param primaryText the primary text to be displayed on the template
     * @param secondaryText the secondary text to be displayed on the template
     * @param image  the url of the image
     * @return Template
     */
    private Template getBodyTemplate3(String title, String primaryText, String secondaryText, Image image) {
        return BodyTemplate3.builder()
                .withImage(image)
                .withTitle(title)
                .withTextContent(getTextContent(primaryText, secondaryText))
                .build();
    }

    /**
     * Helper method to create the image object for display interfaces
     * @param imageUrl the url of the image
     * @return Image that is used in a body template
     */
    private Image getImage(String imageUrl) {
        List<ImageInstance> instances = getImageInstance(imageUrl);
        return Image.builder()
                .withSources(instances)
                .build();
    }

    /**
     * Helper method to create List of image instances
     * @param imageUrl the url of the image
     * @return instances that is used in the image object
     */
    private List<ImageInstance> getImageInstance(String imageUrl) {
        List<ImageInstance> instances = new ArrayList<>();
        ImageInstance instance = ImageInstance.builder()
                .withUrl(imageUrl)
                .build();
        instances.add(instance);
        return instances;
    }

    /**
     * Helper method that returns text content to be used in the body template.
     * @param primaryText
     * @param secondaryText
     * @return RichText that will be rendered with the body template
     */
    private TextContent getTextContent(String primaryText, String secondaryText) {
        return TextContent.builder()
                .withPrimaryText(makeRichText(primaryText))
                .withSecondaryText(makeRichText(secondaryText))
                .build();
    }

    /**
     * Helper method that returns the rich text that can be set as the text content for a body template.
     * @param text The string that needs to be set as the text content for the body template.
     * @return RichText that will be rendered with the body template
     */
    private RichText makeRichText(String text) {
        return RichText.builder()
                .withText(text)
                .build();
    }

}
