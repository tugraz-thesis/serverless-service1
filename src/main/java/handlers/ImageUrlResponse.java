package main.java.handlers;

public class ImageUrlResponse {
    private String url;

    public ImageUrlResponse(String url) {
        this.url = url;
    }

    public ImageUrlResponse() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ImageUrlResponse{" +
                "url='" + url + '\'' +
                '}';
    }
}
